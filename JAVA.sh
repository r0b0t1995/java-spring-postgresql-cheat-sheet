$JAVA_FORM
jFrame = new JFrame("Title");
        jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        jFrame.setContentPane(MainPanel);
        jFrame.setResizable(true);
        jFrame.setVisible(true);
        jFrame.pack();
        jFrame.setSize(1250, 580);
        jFrame.setLocationRelativeTo(null);




$JAVA_STATEMENT_FOR_PROCEDURES
String sql = "CALL insertTeams(?)"
Connection.callableStatement = Connection.connection.prepareCall(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
            Connection.callableStatement.setString(1, this.name);
            Connection.callableStatement.executeUpdate();
            int resultQuery = Connection.callableStatement.getResultSetConcurrency();
            //ResultSet.CONCUR_UPDATABLE = 1008
            if(resultQuery == ResultSet.CONCUR_UPDATABLE){
                r = true;
            }
            Connection.callableStatement.close();



$HOW_DISABLE_CELL_EDITABLE_JTABLE
model = new DefaultTableModel(columnsName, 0){
            @Override
            public boolean isCellEditable(int row, int column) {
                if(column == -1 && row == -1){
                    return true;
                }else{
                    return false;
                }
            }
        };

$JAVA_STATEMENT_FOR_SQLQUERY
#1
String sql = "INSERT INTO cliente VALUES (?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, cliente.getId());
            preparedStatement.setInt(2, cliente.getCedula());
            preparedStatement.setString(3, cliente.getNombre());
            preparedStatement.executeUpdate();
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                cedula = resultSet.getInt(1);
            }
            preparedStatement.close();

#2
String sql = "UPDATE cliente SET cedula = ?, nombre = ?, apellido = ?, direccion = ?, telefono = ?, correo = ? WHERE id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, cliente.getCedula());
            preparedStatement.setString(2, cliente.getNombre());
            preparedStatement.setString(3, cliente.getApellido());
            preparedStatement.setString(4, cliente.getDireccion());
            preparedStatement.setInt(5, cliente.getTelefono());
            preparedStatement.setString(6, cliente.getCorreo());
            preparedStatement.setInt(7, cliente.getId());
            int filasModificadas = preparedStatement.executeUpdate();
            preparedStatement.close();

#3
String sql = "DELETE FROM cliente WHERE id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            int filasModificadas = preparedStatement.executeUpdate();
            preparedStatement.close();

#4
String sql = "SELECT * FROM prestamo WHERE id = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                prestamo = new Prestamo();
                prestamo.setId(resultSet.getInt("id"));
                prestamo.setIdCliente(resultSet.getInt("idcliente"));
                prestamo.setFechaInicio(resultSet.getString("fechainicio"));
                prestamo.setFechaFin(resultSet.getString("fechafin"));
                prestamo.setMontoTotal(resultSet.getInt("montototal"));
                prestamo.setDescripcion(resultSet.getString("descripcion"));
            }
            preparedStatement.close();

#5
List<Prestamo> lista = new ArrayList<>();
        try {
            String sql = "SELECT * FROM prestamo";
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Prestamo prestamo = new Prestamo();
                prestamo.setId(resultSet.getInt("id"));
                prestamo.setIdCliente(resultSet.getInt("idcliente"));
                prestamo.setFechaInicio(resultSet.getString("fechainicio"));
                prestamo.setFechaFin(resultSet.getString("fechafin"));
                prestamo.setMontoTotal(resultSet.getInt("montototal"));
                prestamo.setDescripcion(resultSet.getString("descripcion"));
                lista.add(prestamo);
            }
