    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "roleFk", referencedColumnName = "roleId")
    private Role role;
