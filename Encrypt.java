package Encryption;


import javax.swing.*;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class Encrypt {

    private String convertStingToHex(String plainText){
        StringBuilder stringBuilder = new StringBuilder();
        char[] charArray = plainText.toCharArray();
        for(char c : charArray){
            stringBuilder.append(Integer.toHexString(c));
        }
        return stringBuilder.toString();
    }

    private String encryptMD5(String plainText){
        String stringToHex = convertStingToHex(plainText);
        stringToHex += "!@#$%_:)";
        String md5Hash = "";
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(stringToHex.getBytes());
            byte[] md5Vector = messageDigest.digest();
            byte[] base64Encode = Base64.getEncoder().encode(md5Vector);
            StringBuilder stringBuilder = new StringBuilder();
            for(int i = 0; i < base64Encode.length; i++){
                stringBuilder.append(Integer.toString( (base64Encode[i] & 0xff) + 0x100,16 ).substring(1));
            }
            md5Hash = stringBuilder.toString();
        }catch (NoSuchAlgorithmException e){
            e.printStackTrace();
        }
        return  md5Hash;
    }

    //LENGTH = 128
    public String encryptMethod(String plainText){
        String shaHash = "";
        if(plainText.length() > 0) {
            String md5Hash = encryptMD5(plainText);
            md5Hash += "!encrypt_[:)]!";
            try {
                MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
                byte[] shaVector = messageDigest.digest(md5Hash.getBytes(StandardCharsets.UTF_8));
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < shaVector.length; i++) {
                    stringBuilder.append(Integer.toString((shaVector[i] & 0xff) + 0x100, 16).substring(1));
                }
                shaHash = stringBuilder.toString();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }else{
            JOptionPane.showMessageDialog(null, "Your password doesn't meet the requirements", ":(", JOptionPane.INFORMATION_MESSAGE);
        }
        return shaHash;
    }


    public static void main(String[] args) {
        Encrypt e = new Encrypt();
        System.out.println(e.encryptMethod("robert"));
    }

}
