package Data;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class Connection_DataBase {

    private final String HOST_PORT = "localhost:5432";
    private final String DATABASE = "MyDataBase";
    private final String USER = "postgres";
    private final String PASSWORD = "poiuqwer";
    private final String URL = "jdbc:postgresql://" + HOST_PORT + "/" + DATABASE;
    private final String DRIVER = "org.postgresql.Driver";

    Connection connection = null;
    Statement statement = null;
    ResultSet resultSet = null;
    PreparedStatement preparedStatement = null;


    public void OpenConnection(){
        try{
            Class.forName(DRIVER);
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            statement = connection.createStatement();
            if(connection == null){
                System.err.println("[-] Error with connection");
            }
        }catch (ClassNotFoundException | SQLException e){
            System.err.println("[-] Connection Failed" + e.getMessage());
        }
    }

    public void CloseConnection(){
        try{
            //preparedStatement.close();
            //resultSet.close();
            connection.close();
            statement.close();
        }catch (SQLException sqlException){
            System.err.println("[-] Close Connection Failed");
        }
    }


}
