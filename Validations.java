package Validate;

import javax.swing.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

public class Validations {
    private final String passwordRegex = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%&*_,.=+:/~]).{8,50}$";
    private String warning = "You need to type a password that contains a length of 8, numbers, lower case, upper case and characters";


    public boolean validatePassword(String password){
        boolean r = false;
        if(Pattern.matches(passwordRegex, password)){
            r = true;
        }else{
            JOptionPane.showMessageDialog(null, warning, ":(", JOptionPane.WARNING_MESSAGE);
        }
        return r;
    }

    public Date validateDate(int year, int month, int day){
        Date date = new Date(0000, 00, 00);

        Date actualDate = Date.valueOf(LocalDate.now());
        try {
            date = Date.valueOf(LocalDate.of(year, month, day));
        }catch (DateTimeException | NullPointerException e){
            System.err.println(e.getMessage());
            JOptionPane.showMessageDialog(null, "The date is not correct try again", ":(", JOptionPane.WARNING_MESSAGE);
        }

        if(actualDate.before(date)){
            JOptionPane.showMessageDialog(null, "The date is not correct", ":(", JOptionPane.WARNING_MESSAGE);
            date = null;
        }

        return date;
    }

    public boolean validateItemIsDigit(char item){
        boolean r = true;
            if(Character.isDigit(item)){
                r = false;
            }
        return r;
    }

    public boolean validateUpperLower(char item, boolean lower, boolean upper){
        boolean r = false;
            if(lower && Character.isLowerCase(item) && Character.isLetter(item)){
                r = true;
            }else if(upper && Character.isUpperCase(item) && Character.isLetter(item)){
                r = true;
            }
        return r;
    }


}
