$MAIN_DOCUMENTATION
https://www.postgresql.org/docs/current/sql-commands.html

$SPRING_INITIALIZR
https://start.spring.io/
#Dependencies
{'PostgreSQL Driver'}
{'Spring Web'}
{'Spring Data JPA'}


$DRIVER
https://jdbc.postgresql.org/


$CONNECT_TO_SPRING_BOOT_JAVA
#NOTE: `put into file src/main/resources/application.propierties`
spring.datasource.url=jdbc:postgresql://localhost:5432/'DATABASE_NAME'
spring.datasource.username='USER_NAME'
spring.datasource.password='PASSWORD'
spring.jpa.hibernate.ddl-auto=create-drop
spring.jpa.show-sql=true
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQLDialect
spring.jpa.properties.hibernate.format_sql=true


$SPRING_DEPENDENCY
#DRIVER
<dependency>
	<groupId>org.postgresql</groupId>
	<artifactId>postgresql</artifactId>
	<scope>runtime</scope>
</dependency>

#CONNECT TO POSTGRESQL DATABASE WITH SPRING DATA JPA
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>
